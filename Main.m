clear;
clc;
global VO2_array VATPase_array Time_array Vhyd_array VATPase_array2 VMiCK_array VMMCK_array Cai_array PCrDiff_array ATPDiff_array
VO2_array=[]; 
VATPase_array=[]; 
Time_array=[];
Vhyd_array=[];
VATPase_array2=[];
VMiCK_array=[];
VMMCK_array=[]; 
Cai_array =[];
PCrDiff_array=[];
ATPDiff_array=[];

getSimParams;
simParams.AcCoA = 0.1; 
simParams.GLU = 30; 

simParams2=simParams;
simParams2.rhoRENscale=simParams.rhoRENscale*0.45;    %complexI   %%%simulate mtCaMKII
simParams2.rhoSDH=simParams.rhoSDH*1.3;   %complex II
simParams2.KCS=simParams.KCS*1; 
simParams2.kfACO=simParams.kfACO*1;  
simParams2.kfidh=simParams.kfidh*1.2; 
simParams2.kfKG= simParams.kfKG*1.2;   
simParams2.KfSL=simParams.KfSL*1; 
simParams2.kfFH= simParams.kfFH*1.2;   
simParams2.kfMDH=simParams.kfMDH*1.2;   
simParams2.kfAAT=simParams.kfAAT*1; 
simParams2.F_MiCK=simParams.F_MiCK*0.8;     
simParams2.ATPhydAmp=simParams.ATPhydAmp*1;
simParams2.CaiAmp=simParams.CaiAmp*1.05;  %simulate mtCaMKII  Calcium
simParams2.CaiRest=simParams.CaiRest*1.2;  %%%simulate mtCaMKII diastolic Calcium



tic
SimTime= 300E3;
SS01=[6.77465251802724e-05,0.0872689651343473,165.885501369421,0.956731373387505,2.92865176286671e-05,0.620218062672645,0.115853508907814,0.130008382547870,0.0523627985502977,0.0224610796480669,0.159462724585035,0.149748809418467,0.00389290780264777,0.0990855696942846,9.23576950771275e-06,2.93832169609442e-08,0.000396023463516576,3.76537142825079e-07,0.00382373555862163,0.0167023801206990,2.98688504074758,0.0137188089202660,0.0497011360520279,0.000999912713973360,0.000450987101223550,1.90927176409956,0.110700345772471,0.0244900665418451,0.0244620046264031,0.0217784644894968,1.90929981747040,0.226892789790524,0.0688373416471216,0.0282696475519169,0.192956255034324,0.277607632353568,0.240196514032033,0.0761535685661106,3.36855307775590,5.54478098665225,8.07449678708275,8.06942190651997,0.0682155752251641,3.37649107109685,5.54990496633574,1.59869020160361,1.60183303600056,1.04692854286567];
options= odeset('RelTol',1e-4,'AbsTol',1e-7); 
[t__, X__] = ode15s(@MitoModel, [0 SimTime], SS01, options,  simParams);
VATPASE=VATPase_array;
VO2=VO2_array;
Time=Time_array;
Vhyd=Vhyd_array;
VATPasemito=VATPase_array2;
VMiCK=VMiCK_array;
VMMCK=VMMCK_array;
Cai1=Cai_array;
PCrDiff1=PCrDiff_array;
ATPDiff1=ATPDiff_array;
VMiCK_array=[];
VMMCK_array=[]; 
VO2_array=[]; 
VATPase_array=[]; 
Time_array=[];
Vhyd_array=[];
VATPase_array2=[];
VMiCK_array=[];
VMMCK_array=[]; 
Cai_array =[];
PCrDiff_array=[];
ATPDiff_array=[];
TIMER1=toc

%mitoCaMKII group
SS02=[7.85634362575536e-05,0.191478578884998,162.153963284581,0.958640990521283,5.53775457533998e-05,8.22289180467453,0.102661398225130,0.0916166058091741,0.00186020016284664,0.0307833405070096,0.213150301663947,0.204905944562215,0.00352837841242426,0.0994816629576714,2.22171769060198e-06,1.12010264456806e-08,1.67866694143900e-05,3.84846424028647e-08,0.530825357009280,0.0242869081954113,2.71896314034616,0.0246139050130743,0.0499697556688799,0.000968592156176647,0.000412839773839470,1.97706578219650,0.0268417628450429,0.00520815814587361,0.00517951945820134,0.00861282267645714,1.97709441667388,0.278846436707971,0.0322227101173211,0.0137569610080858,0.260962020974764,0.307542074587836,0.291733168233811,0.158144659802549,3.21859792419766,8.35785818411191,5.26141876853048,5.25648082023862,0.150576824876021,3.22616575912415,8.36284390670789,3.36833800094771,3.37148367765155,0.942718929115014];
[t2__, X2__] = ode15s(@MitoModel, [0 SimTime], SS02, options,  simParams2);  %simulate lower Complex I, higher TCA and complex II
VATPASE2=VATPase_array;
VO22=VO2_array;
Time2=Time_array;
Vhyd2=Vhyd_array;
VATPasemito2=VATPase_array2;
VMiCK2=VMiCK_array;
VMMCK2=VMMCK_array;
Cai2=Cai_array;
PCrDiff2=PCrDiff_array;
ATPDiff2=ATPDiff_array;
VMiCK_array=[];
VMMCK_array=[]; 
VO2_array=[]; 
VATPase_array=[]; 
Time_array=[];
Vhyd_array=[];
VATPase_array2=[];
Cai_array =[];
PCrDiff_array=[];
ATPDiff_array=[];

TIMER2=toc

%mitoCaMKII x CKmito group
simParams3=simParams2;
simParams3.F_MiCK=simParams.F_MiCK*1.2; 
simParams3.ATPhydAmp=simParams.ATPhydAmp*1;        
simParams3.rhoRENscale=simParams.rhoRENscale*0.7; 
simParams3.CaiAmp=simParams.CaiAmp*1; 
simParams3.CaiRest=simParams.CaiRest*1; 
SS03=[6.83125556566247e-05,0.0946500739254374,163.065088055817,0.965419586923341,3.41050143104232e-05,1.64849439271904,0.118570286511899,0.116900675608519,0.0184192230813421,0.0256720845315329,0.178653242948858,0.170459098511890,0.00389338178388870,0.0993844458580036,4.90012555531063e-06,1.78983228626688e-08,0.000134121288299225,1.45469313642461e-07,0.00437582708658200,0.0193678247429386,2.98522153588663,0.0216925644250919,0.0498851878869179,0.000999876633400913,0.000411494010233231,1.94445722644525,0.0706052047365670,0.0129710504408068,0.0129425880068603,0.0145407116868580,1.94448568168506,0.251875276689974,0.0508267304552417,0.0217999654928273,0.217622235219781,0.289878383216684,0.260188303396525,0.0883697351101573,3.28837284790657,5.81039026324706,7.80888035309067,7.80263552972506,0.0727476021005414,3.30399498091618,5.81669550482998,1.75962868123910,1.76277293924017,1.03954743407456];
[t3__, X3__] = ode15s(@MitoModel, [0 SimTime], SS03, options,  simParams3);  
VATPASE3=VATPase_array;
VO23=VO2_array;
Time3=Time_array;
Vhyd3=Vhyd_array;
VATPasemito3=VATPase_array2;
VMiCK3=VMiCK_array;
VMMCK3=VMMCK_array;
Cai3=Cai_array;
PCrDiff3=PCrDiff_array;
ATPDiff3=ATPDiff_array;
VMiCK_array=[];
VMMCK_array=[]; 
VO2_array=[]; 
VATPase_array=[]; 
Time_array=[];
Vhyd_array=[];
VATPase_array2=[]; 
Cai_array =[];
PCrDiff_array=[];
ATPDiff_array=[];
TIMER3=toc

%CKmito group
simParams4=simParams;
simParams4.F_MiCK=simParams.F_MiCK*1.2;  %overexpress mtCK   %%%CKmito simulate 
simParams4.rhoRENscale=simParams.rhoRENscale*1; 
simParams4.CaiAmp=simParams.CaiAmp*1; 
simParams4.CaiRest=simParams.CaiRest*1; 
SS04=[6.77609613844415e-05,0.0846255875396705,165.970483475126,0.956629412239742,2.93888902937523e-05,0.646053141322661,0.115859286355081,0.129829227546270,0.0518593174606072,0.0225230512346216,0.159666721839678,0.149945792569336,0.00390251895130888,0.0990859498628442,9.22246028290489e-06,2.94191357027562e-08,0.000395266756783962,3.76026240450332e-07,0.00382420286493815,0.0166185733339563,2.98692868499770,0.0137443457084426,0.0497015442165619,0.000999913010163650,0.000452807009481135,1.90966105685379,0.110097214174148,0.0244099952978142,0.0243819169942881,0.0217631526316174,1.90968912705097,0.227046612921702,0.0687647181822329,0.0281929699244122,0.193185233764335,0.277731908606647,0.240392352386715,0.0803153797509980,3.29642720324906,5.48988265129354,8.12938958598750,8.12335709563138,0.0661145480419660,3.31062803495809,5.49597350592840,1.60309407769649,1.60623901932177,1.04957192046034];
[t4__, X4__] = ode15s(@MitoModel, [0 SimTime], SS04, options,  simParams4); 
VATPASE4=VATPase_array;
VO24=VO2_array;
Time4=Time_array;
Vhyd4=Vhyd_array;
VATPasemito4=VATPase_array2;
VMiCK4=VMiCK_array;
VMMCK4=VMMCK_array;
Cai4=Cai_array;
PCrDiff4=PCrDiff_array;
ATPDiff4=ATPDiff_array;
TIMER4=toc


%=====================Calculation=============================
ADPtotal1=(X__(:,2)+X__(:,38)+X__(:,43));  %sum 
ADPtotal2=(X2__(:,2)+X2__(:,38)+X2__(:,43));
ADPtotal3=(X3__(:,2)+X3__(:,38)+X3__(:,43));
ADPtotal4=(X4__(:,2)+X4__(:,38)+X4__(:,43));

ATPtotal1=(X__(:,48)+X__(:,39)+X__(:,44));  %unit 
PrCtotal1=(X__(:,41)+X__(:,42));
ATPtotal2=(X2__(:,48)+X2__(:,39)+X2__(:,44));
PrCtotal2=(X2__(:,41)+X2__(:,42));
ATPtotal3=(X3__(:,48)+X3__(:,39)+X3__(:,44));
PrCtotal3=(X3__(:,41)+X3__(:,42));
ATPtotal4=(X4__(:,48)+X4__(:,39)+X4__(:,44));
PrCtotal4=(X4__(:,41)+X4__(:,42));


ATPtotal_arr=[ATPtotal1(end)  ATPtotal2(end) ATPtotal3(end) ATPtotal4(end)];
ADPtotal_arr=[ADPtotal1(end)  ADPtotal2(end) ADPtotal3(end) ADPtotal4(end)];
PrCtotal_arr=[PrCtotal1(end)  PrCtotal2(end) PrCtotal3(end) PrCtotal4(end)];
ATPADP=ATPtotal_arr./ADPtotal_arr;
PrCATP=PrCtotal_arr./ATPtotal_arr;



A_ATPmito=[X__(end,48), X2__(end,48), X3__(end,48), X4__(end,48)];
A_ATPcyto=[X__(end,44), X2__(end,44), X3__(end,44), X4__(end,44)];
A_ATPims=[X__(end,39), X2__(end,39), X3__(end,39), X4__(end,39)];



%calculate ratio ATP/PCr, ATP/ADP
ATPPCrims=[X__(end,41)/X__(end,39), X2__(end,41)/X2__(end,39),X3__(end,41)/X3__(end,39),X4__(end,41)/X4__(end,39)  ] ;%PCrims/ATPims
ATPPCrcyto=[X__(end,42)/X__(end,44), X2__(end,42)/X2__(end,44),X3__(end,42)/X3__(end,44),X4__(end,42)/X4__(end,44)  ] ;%PCrcyto/ATPcyto
ATPADPmito=[X__(end,48)/X__(end,2), X2__(end,48)/X2__(end,2),X3__(end,48)/X3__(end,2),X4__(end,48)/X4__(end,2)  ];
ATPADPcyto=[X__(end,44)/X__(end,43), X2__(end,44)/X2__(end,43),X3__(end,44)/X3__(end,43),X4__(end,44)/X4__(end,43)  ];
 


ratios= [PrCATP;ATPPCrcyto;ATPPCrims ; ATPADP; ATPADPcyto; ATPADPmito];

%free form bound form
pH_p = 7.0;
Hi = 10^(-pH_p+3);

KaPi=1.78E-7;
KaATP=3.31E-7;
KMgATP=6.46E-5;
KaADP=4.17E-7;
KMgADP= 5.62E-4;
Mg=0.4;  %0.4-1mM

A_ADPcyto=[X__(end,43), X2__(end,43),X3__(end,43),X4__(end,43)];
A_ADPcyto_Free = A_ADPcyto/( 1+ Hi/1000/KaADP + Mg/1000/ KMgADP);
A_ATPcyto=[X__(end,44), X2__(end,44),X3__(end,44),X4__(end,44)];

Ratio_ATP_ADPfree=A_ATPcyto./A_ADPcyto_Free;


figure;
bar(Ratio_ATP_ADPfree, 0.4)
title('ATP/ADPfree')
ylabel('ATPtotal / ADP_free Ratio')
set(gca, 'XTickLabel', {'WT', 'mtCaMKII', 'mtCaMKIIxCKmt', 'CKmt'});
xtickangle(45)


figure;
bar(PrCATP, 0.4)
title('PCr/ATP')
ylabel('Ratio')
set(gca, 'XTickLabel', {'WT', 'mtCaMKII', 'mtCaMKIIxCKmt', 'CKmt'});
xtickangle(45)




