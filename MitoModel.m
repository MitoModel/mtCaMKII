function dydt = MitoModel(t,X,simParams)
global VO2_array VATPase_array Time_array Vhyd_array VATPase_array2 VMiCK_array VMMCK_array Cai_array PCrDiff_array ATPDiff_array
%Code adapted from JHU ICM Mito-ROS_Production-and-Scavenging Model ( http://gforge.icm.jhu.edu/gf/project/etcrosmodel/docman/?subdir=141    Mito-ROS_Production-and-Scavenging)
% Model and code based on Doyle et al.  Biophys J. 2013 Dec 17;105(12):2832-42. 
% Doyle et al. Biophys J. 2013 Aug 20; 105(4): 1045V1056.; 
% Van  Beek et al.2007 



Dpsi = X(3);
NADH = X(4);
Hm = X(5);
ISO = X(7);
aKG = X(8);
SUC = X(10);
FUM = X(11);
OAA = X(13);
NADPHm = X(14);
GSSGm = X(21);

C4_tot = .325; %nmol/mg pn

Q_n = X(26);
Qdot_n = X(27);
QH2_n = X(28);
QH2_p = X(29);
Qdot_p = X(30);
Q_p = X(31);
b1 = X(32);  %ox-ox
b2 = X(33);  %bL_red-bH_ox
b3 = X(34);  %bL_ox-bH_red
b4 = 0.325-b1-b2-b3;  %red-red
FeS_ox = X(35);
FeS_red = 0.325-FeS_ox;
c1_ox = X(36);
c_ox = X(37);
c1_red = 0.325 - c1_ox;
c_red = C4_tot - c_ox;


% pacing protocol cytosolic ATPhydrolysis
Tcycle = 120;
AWidth = Tcycle/3; 
hWidth= AWidth/2;   %1/6
Amp= simParams.ATPhydAmp;
tmod=mod(t,Tcycle);
 	if tmod <  hWidth
 		reaction_ATPase=Amp*tmod/Tcycle*6;
    elseif tmod < AWidth
 		reaction_ATPase=Amp*(1-6*(tmod/Tcycle-1/6));
    else
            reaction_ATPase=0;
    end
    

CaAWidth = Tcycle; 
CahWidth= CaAWidth*0.3;  
CaAmp= simParams.CaiAmp;  
Cai_rest=simParams.CaiRest; 
 	if tmod <  CahWidth
 		Cai=tmod*(CaAmp-Cai_rest)/CahWidth+Cai_rest;
   
    else
        
        Cai=CaAmp-(CaAmp-Cai_rest)/(Tcycle-CahWidth)*(tmod-CahWidth);    %CaAmp*(1-tmod/Tcycle)+Cai_rest; 
           
    end
      

VmPiC=4.9; 
RhoF1=5;   
VmANT=3.15;  

EtCuZnSOD= 2.4e-004;
EtMnSOD=0.0003;%5.591491516965823210842e-4;
EtGPXm= 1e-4; %5e-5;
Etcat=1e-6;
EtGPX=5e-5;


Etxrm = simParams.Etxrm;
Etxr = simParams.Etxr;
EtGR = simParams.EtGR;
EtGRm = simParams.EtGRm;

RT_over_F = 26.708186528497409326424870466321;
F_over_RT = 1/RT_over_F;


AcCoA = simParams.AcCoA;
GLU = simParams.GLU;
gh=simParams.gh;
RhoREN=0.2;
RhoSDH=simParams.rhoSDH; 


VNam=-2/3.0E-4*X(1)-X(2)-1.812E-3*Dpsi-Hm/1.0E-5+X(6)+1.01E1;
kresf=5.765E13;     %eq cte of SHH
KeqF1=1.71E6;       %eq cte ATP synthes
Catp= 1.01;         


ADP_cyto= X(43); %AW 06/2017   
ATP_cyto=X(44);   %AW 06/2017  
Pi_cyto=X(47); %Pi AW 06/2017   
ATPm=X(48); 

beta_matr=1e-5;     %1E-5 %Mitochondrial buffering capacity
Cm=1.5;             %total cyto adenon mito
Conc_NHE=0.00785;    %concentration NHE -- same as An-Chi's

KCS=simParams.KCS;   
kfACO=simParams.kfACO;  
kfidh=simParams.kfidh;          %0.0163; 
kfKG=simParams.kfKG;        %8.8382E-4;    
KfSL=simParams.KfSL;         % 0.005; 
kfFH=simParams.kfFH;        %3.5E-4;   
kfMDH=simParams.kfMDH;        %0.0062163;  
kfAAT=simParams.kfAAT;     %1.28E-3;  

Hi=1.0E-4;  %same
KaidADP=6.2E-1; %same
KaidCa=5.0E-4; %same
KaPi=1.78E-7;
KaATP=3.31E-7;
KMgATP=6.46E-5;
KaADP=4.17E-7;
KMgADP= 5.62E-4;
KaSUC=6.3E-6;
KaH2O=1.0E-14;
KSLeq=3.115;
kh_1=1E-5;          
kh_2=9E-4;
kh_1a=4E-5;
kh_2a=7E-5;
khm1=1.131E-5;
khm2=2.67E1;
khm3=6.68E-9;
khm4=5.62E-6;
kmoff=3.99E-2;
Mg=0.4; %mitochondrial -- same as An-Chi
KkgCa= 1.5E-4; %same
VNai=10.0;           
Vmuni= 0.0015; %0.004459;      %Wei V uni 0.002459 mM ms-1
VmNaCa = 18.33e-5;      %1.0E-4; Wei V NaCa 9.33.10-5 mM ms-1

H_mito_1000= Hm/1000;
Mg_1000=Mg/1000;
ATP4 = ATPm / ( 1+ H_mito_1000/KaATP + Mg_1000/ KMgATP);
ATPMg= ATP4 * Mg_1000 / KMgATP;
HATP=  ATP4 * H_mito_1000 / KaATP ;
ADP3=  X(2) / ( 1+ H_mito_1000/KaADP + Mg_1000/ KMgADP); 
ADPMg= ADP3 * Mg_1000/KMgADP;
HADP=  ADP3 * H_mito_1000/KaADP ;
H2Pi = X(6)/(1+ KaPi/H_mito_1000);
HPi= H2Pi*KaPi/(H_mito_1000);
ATP4_cyto= (Catp-ADP_cyto) / (1+ Hi/1000/KaATP + 1/1000/KMgATP);
ADP3_cyto= (ADP_cyto)/ (1+ Hi/1000/KaADP + 1/1000/KMgADP);   
polyATP=1+ H_mito_1000/KaATP + Mg_1000/KMgATP;
polyADP=1+ H_mito_1000/KaADP + Mg_1000/KMgADP;
polyPi = 1+ H_mito_1000/KaPi; 
polyH2O =1+ H_mito_1000/KaH2O;
SUC_poly=1+ H_mito_1000/KaSUC;
kgrxm=3.6E-4;       %in (ms-1) en VGRXm
kgrx=3.6E-4;        %in (ms-1) en VGRX
KeqGRX=1.37E-3;     %en en VGRXm y en VGRX
GrxT= 0.002;         %en en VGRXm y en VGRX
PSSGT= 0.001;          %PSSG total; estimated  %en en VGRXm y en VGRX
kcatPSH= 0.64;         %kcat de enzima de PSH + GSH
EtPSH=0.8e-3;          %concentracion de enz
kmGSH=0.75;            %km de GSH
kactH2O2=1e-3;         %k activation of H2O2 
KmGrx=0.01 ;        %estimated %en en VGRXm y en VGRX
KmPSSG= 0.0005;     %estimated  %en en VGRXm y en VGRX


c_difH2O2=2.0E-4;%ORIGINAL 8.0E-3
c_VGST=1.5E-8;%ORIGINAL 1.5E-8;
%EtGRm=1e-004;

kcat=1.7E1;
%Etcat=5.0E-4;
%EtGPX=1.E-2;
kGR=2.5E-3;
%EtGR=5.0E-3;

kcXR= 22.75E-3;     %FLAG


NADPHc=7.5E-2;
Vem= 2.06E-5;
Vm= 5.17E-6;
VATPm =Cm-X(2);
NAD=1.0-NADH;
VATPi=Catp-ADP_cyto;  %??AW 06/2017
DpH=-log10(1.0E-4)+log10(Hm);
muH=-2.303*2.670818E1*DpH+Dpsi;
VAREN=1.35E18*sqrt(NADH/NAD);
VNOden=1/((exp(3.0E2/2.670818E1)+2.077E-18*exp(3.0E2/2.670818E1)*VAREN)+(1.728E-9+1.059E-26*VAREN)*exp(5.1/2.670818E1*muH));
VHNe=6*RhoREN*(6.394E-13*VAREN-(6.394E-13+1.762E-16)*exp(5.1/2.670818E1*muH))*VNOden;                                                           %not use?
VNO=0.5*RhoREN*((6.394E-13+2.656E-22*exp(3.0E2/2.670818E1)+8.632E-30*exp(5.1/2.670818E1*muH))*VAREN-6.394E-13*exp(5.1/2.670818E1*muH))*VNOden;  %not use?
Kresf_app=kresf/SUC_poly;
VARSDH=2.670818E1*(log(Kresf_app*sqrt(X(10)/X(11))));
VHSDHden=RhoSDH/((1+2.077E-18*exp(VARSDH/2.670818E1))*exp(2.0E2/2.670818E1)+(1.728E-9 ...
                + 1.059E-26*exp(VARSDH/2.670818E1))*exp(3.4/2.670818E1*muH))/(1+X(13)/1.5E-1);
VHSDH=4*(6.394E-13*exp(VARSDH/2.670818E1)-(6.394E-13+1.762E-16)*exp(3.4/2.670818E1*muH))*VHSDHden;
KATPase_app=KeqF1*H_mito_1000*polyATP*polyH2O/(polyADP*polyPi);
VAF1=KATPase_app/X(6)*(ATPMg/(ADP3+HADP)); %VAF1=KATPase_app/X(6)*(ATPMg/(ADP3+HADP));  %AW 07/16/2017
VHuden=-RhoF1/(exp(1.5E2/2.670818E1)+1.346E-4*exp(1.5E2/2.670818E1)*VAF1+(7.739E-7+6.65E-15*VAF1)*exp(3/2.670818E1*muH));
VATPase=((1.656E-6+9.651E-17*exp(1.5E2/2.670818E1)-4.585E-17*exp(3/2.670818E1*muH))*VAF1-1.656E-8*exp(3/2.670818E1*muH))*VHuden;
Vhu=(3.0E2*1.656E-8+3.0E2*1.656E-8*VAF1-3*(1.656E-8+3.373E-10)*exp(3/2.670818E1*muH))*VHuden;
VANT=VmANT*(1-(ATP4_cyto*ADP3)/(ADP3_cyto*ATP4))*exp(-Dpsi/2.670818E1)/((1+ATP4_cyto/(ADP3_cyto)*exp(-1.8721E-2*Dpsi))*(1+ADP3/(ATP4)));    %@@@@@@@@@@@@@@@@@
V2FRTdsi=2*(Dpsi-9.1E1)/2.670818E1;
Vuniden=(((1+Cai/1.9E-2)^4)+1.1E2/((1+Cai/3.8E-4)^(2.8)))*(1-exp(-V2FRTdsi));
Vuni=(Vmuni/1.9E-2*Cai*V2FRTdsi*(1+Cai/1.9E-2)^3)/Vuniden;
VnaCa=VmNaCa*exp(2.5E-1*V2FRTdsi)*X(1)/(Cai*((1+9.4/VNai)^3)*(1+3.75E-4/X(1)));
Vhleak=gh*muH;
VCS = (KCS*4.0E-1*AcCoA/(1.26E-2+AcCoA))*X(13)/(X(13)+6.4E-4);
VACO =kfACO*(1.3-aKG-X(9)-X(10)-X(11)-X(12)-X(13)-ISO*1.45045);
Va=1/((1+ADP3/KaidADP)*(1+X(1)/KaidCa));
Vi=1+NADH/1.9E-1;
Vb=1.0+Hm/kh_1+kh_2/Hm;
VIDH=kfidh*1.09E-1/(Vb+9.23E-1/NAD*Vi+((1.52/ISO)^2)*Va*(1+9.23E-1/NAD*Vi));
VKGa=1/((1+1.2987E1)*(1+X(1)/KkgCa));  %Mg/KdMG = .4/.0308 = 12.987
VKGDH=kfKG*5E-1/(1.0 + Hm/kh_1a+kh_2a/Hm+VKGa*(3.0E1/aKG)^1.2+VKGa*(3.87E1/NAD));
KSLeq_app = KSLeq * SUC_poly * polyATP / ( polyADP * polyPi );
VSL=KfSL*(X(9)*X(6)*X(2)-X(10)*(ATP4+HATP)*2.0E-2/KSLeq_app); %0.02 is [CoA]
VO2SDH=5.0E-1*((6.394E-13+2.656E-22*exp(2.0E2/2.670818E1))*exp(VARSDH/2.670818E1)...
        -(6.394E-13-8.632E-30*exp(VARSDH/2.670818E1))*exp(3.4/2.670818E1*muH))*VHSDHden;
VFH=kfFH*(X(11)-X(12)/1);
V26=(1/(1+khm3/Hm+khm3*khm4/(Hm^2)))^2;
V27=1/(1+Hm/khm1+(Hm^2)/(khm1*khm2))+kmoff;
VMDH=kfMDH*V26*V27*1.54E-1/(1+1.493/X(12)*(1+X(13)/3.1E-2)+2.244E-1/NAD+1.493/X(12)*(1+X(13)/3.1E-2)*2.244E-1/NAD);
VAAT=1.5E-6*6.6*GLU*X(13)/(1.5E-6*6.6/kfAAT+aKG); %1.5e-6 is kASP, 6.6 is KE_AAT
VBeta1p=(1.6E-1*2.4E1*Hi)/(1.585E-4*2.4E1+2.4E1*Hi+1.585E-4*VNai);
VBeta1n=(9.39E-2*2.4E1*Hm)/(1.585E-4*2.4E1+2.4E1*Hm+1.585E-4*VNam);
VBeta2p=(2.52E-2*1.585E-4*VNam)/(1.585E-4*2.4E1+2.4E1*Hm+1.585E-4*VNam);
VBeta2n=(4.29E-2*1.585E-4*VNai)/(1.585E-4*2.4E1+2.4E1*Hi+1.585E-4*VNai);
VpHm=-log10(Hm)+3;
VNaH=Conc_NHE*(((VBeta1p*VBeta2p)-(VBeta1n*VBeta2n))/(VBeta1p+VBeta1n+VBeta2p+VBeta2n))/(1+10^(3*(VpHm-8.52)));
VPiC=VmPiC/60000*(9.0E1*Pi_cyto*1.0E-8/Hm/(1.106E1*4.084E-5)-9.0E1*H2Pi*1.0E-4/(1.106E1*4.084E-5))/(1+Pi_cyto/1.106E1+...
    1.0E-8/Hm/4.084E-5+Pi_cyto*1.0E-8/Hm/(1.106E1*4.084E-5)+H2Pi/1.106E1+1.0E-4/4.084E-5+H2Pi*1.0E-4/(1.106E1*4.084E-5));

kfIDH_NADP = 8.72e-5; %from Popova et al. 8.72e-5; %original
%kfIDH_NADP = 8.72e-8; %8.72e-7; %8.72e-6; %4.35e-5; %6.54e-5;
krIDH_NADP = 5.45E-6; %from Popova et al 5.45E-6; %original
%krIDH_NADP = 5.45e-9; %5.45e-8; %5.45e-7; %2.725e-6; %4.088e-6;
Km_IDH2_H = 0.5;
Km_IDH2_ISO = 0.045; %from Popova  et al.  %was 3.9e-3;
Km_IDH2_NADP = 0.046; %from Popova et al. %was 6.7e-3
Ki_IDH2_NADP = 2e-6;
Km_IDH2_NADPH = 1.2e-2;
Ki_IDH2_aKG = 0.080; %0.51; %0.080; %from Popova et al. %was 0.51

%IDH2 equations
VNADPm=1.0E-1-NADPHm;

%Kembro et al. version
VdenID_NADP=(1+X(5)/5.0E-1)*(1+X(7)/3.9E-3+VNADPm/6.7E-3*(1+2.0E-6/VNADPm)+X(8)/5.1E-1+X(14)/1.2E-2+X(7)/3.9E-3*VNADPm/6.7E-3*(1+...
            +2.0E-6/VNADPm)+X(8)/5.1E-1*X(14)/1.2E-2+X(7)/3.9E-3*X(14)/1.2E-2+X(8)/5.1E-1*VNADPm/6.7E-3*(1+2.0E-6/VNADPm));
VIDH_NADP=(8.72E-5*X(7)/3.9E-3*VNADPm/6.7E-3*(1+2.0E-6/VNADPm)-5.45E-6*X(8)/5.1E-1*X(14)/1.2E-2)/VdenID_NADP;

%THD equations
Et_THD = 11e-5; %1.187e-5;
Kf_THD = 1.17474;
Kr_THD =  17.2756; %10;
KmNADP = 1.7e-2; %5e-2; %1.7E-2;
KmNAD = 1.25e-1;
KmNADPH = 2e-2; %2e-2;
KmNADH = 1e-2;
d = 0.5;
x = 0.1;

%Kembro et al. version
VDNADP=exp(3.7441E-3*muH);
VDNAD=exp(1-3.7441E-3*muH);
VTHDen=1+X(4)/1.0E-2+NAD/1.25E-1+VNADPm/1.7E-2+X(14)/2.0E-2+X(4)/1.0E-2*VNADPm/1.7E-2*VDNADP+X(14)/2.0E-2*X(4)/1.0E-2*VDNAD+...
            +NAD/1.25E-1*VNADPm/1.7E-2*VDNADP*VDNAD+X(4)/1.0E-2*X(14)/2.0E-2;
VTHD=(1.1875E-5*1.174737*X(4)/1.0E-2*VNADPm/1.7E-2*VDNADP-1.1875E-4*NAD/1.25E-1*X(14)/2.0E-2*VDNAD)/VTHDen;

%-----------begin scavenging equtaions-------------------------
VIMAC=(1.0E-3+1.0E4/(1+1.0E-2/X(16)))*(3.5E-8+3.9085E-6/(1+exp(7.0E-2*(4.0+Dpsi))))*Dpsi;
VtrROS=-1.0E-1*(-Dpsi-2.6730818E1*log(X(15)/X(16)))/Dpsi*VIMAC;
VMnSOD=2*1.2E3*2.5E-4*(1.2E3+2.4E1*(1+X(17)/5.0E-1))*EtMnSOD*X(15)/(2.5E-4*(2*1.2E3+2.4E1*(1+X(17)/5.0E-1))+1.2E3*2.4E1*(1+X(17)/5.0E-1)*X(15));
VCuZnSOD=2*1.2E3*2.5E-4*(1.2E3+2.4E1*(1+X(18)/5.0E-1))*EtCuZnSOD*X(16)/(2.5E-4*(2*1.2E3+2.4E1*(1+X(18)/5.0E-1))+1.2E3*2.4E1*(1+X(18)/5.0E-1)*X(16));

Phi1 = 5e-3; %5e-5; %5e-3;
Phi2 = 0.75; %75; %7.5e-1;
VGPXm=EtGPXm*X(17)*X(19)/(Phi1*X(19)+Phi2*X(17));
Km_GRm_NADPH = 1.5e-2; %2.1e-2; %1.5e-2; %was 1.5e-2, %from Carlberg and Mannervik for calf liver = 21uM
VGRm=kGR*EtGRm/(1+6.0E-2/GSSGm+Km_GRm_NADPH/NADPHm+6.0E-2/GSSGm*Km_GRm_NADPH/NADPHm);

VGPX=EtGPX*X(18)*X(20)/(Phi1*X(20)+Phi2*X(18));
GSH_tot = 6.0;
%GSH_tot = 0.6;
VGSS=5.0E-1*(GSH_tot-X(19)-X(20)-2*GSSGm-X(24)-X(25));
% %under the approximation that GSH transport is off
% GSHi_tot = 2;
% VGSS = 0.5*(GSHi_tot-X(20)); %!!!

Km_GRi_NADPH = 1.5e-2; %was 1.5e-2
VGR=kGR*EtGR/(1+6.0E-2/VGSS+Km_GRi_NADPH/NADPHc+6.0E-2/VGSS*Km_GRi_NADPH/NADPHc);

%protein glutathionylation
VGRXm=kgrxm*KeqGRX*X(19)^2*GrxT*X(24)/((GSSGm+KeqGRX*X(19)^2)*(KeqGRX*X(19)^2*GrxT/(GSSGm+KeqGRX*X(19)^2)+KmGrx)*(X(24)+KmPSSG));
VGRX=kgrx*KeqGRX*X(20)^2*GrxT*X(25)/((VGSS+KeqGRX*X(20)^2)*(KeqGRX*X(20)^2*GrxT/(VGSS+KeqGRX*X(20)^2)+KmGrx)*(X(25)+KmPSSG));
% VGRXm = 0; %!!!
% VGRX = 0;

EoTxPXm=0.003;		%3e-3 in paper
EoTxPX=0.1;        %0.1 in paper
Phi2Trx= 1.85;      %from Sztajer et al., JBC 2001 vol 276 7397-7403 % in VTxPxm and VTxPx
Phi1Trx= 3.83;       %3.83 in paper, in VTxPxm and VTxPx
TrxTm=0.025;       % en VTxRm; total amount of TrsSH2m
TrxT= 0.05 ;        % en VTxR; total amount of TrsSH2i
KMTrxSS= 0.035;     %FLAG
KMnadph= 0.012;		%Trxi and Trxm Km for NADPH
TrxSSm=TrxTm-X(22);
TrxSS=TrxT-X(23);

%thioredoxin peroxidases
VTxPXm = EoTxPXm*X(17)*X(22)/(Phi2Trx*X(22)+Phi1Trx*X(17));
VTxPX = EoTxPX*X(18)*X(23)/(Phi2Trx*X(23)+Phi1Trx*X(18));

%thioredoxin reductases
VTxRm = Etxrm*kcXR/(1+KMTrxSS/TrxSSm+KMnadph/NADPHm+KMTrxSS/TrxSSm*KMnadph/NADPHm);
VTxR = Etxr*kcXR/(1+KMTrxSS/TrxSS+KMnadph./NADPHc+KMTrxSS/TrxSS*KMnadph./NADPHc);

VdifH2O2=c_difH2O2*(X(17)-X(18));

Vcat=2*kcat*Etcat*X(18)*exp(-5.0E-2*X(18));
VGST=c_VGST*(X(20)-X(19))/(X(20)+2.6);
% VGST = 0; %!!!
VPSSGm=kcatPSH*EtPSH*(PSSGT-X(24))/(1+(kmGSH/X(19))/(1+(X(17)/kactH2O2)));
VPSSGi=kcatPSH*EtPSH*(PSSGT-X(25))/(1+(kmGSH/X(20))/(1+(X(18)/kactH2O2)));
% VPSSGm = 0; %!!!
% VPSSGi = 0;

%-----------ETC model----------------------------
%--------------complex I-------------------------   %%%%%%%
%100412-betterC1params
parameterTrial = [0.114147820723305,1.59784103254246,9.99991429659771,61.5385020075147,114.118151557370,0.0206803977208407,16.3056727383927,2.91670197986420,0.326041267378776,22.1225360173174;];

fwd1 = parameterTrial(1);  %a23
fwd2 = parameterTrial(2);  %a34
fwd3 = parameterTrial(3);  %a47
fwd4 = parameterTrial(4);  %a75
rev1 = parameterTrial(5);  %a32
rev2 = parameterTrial(6);  %a43
rev3 = parameterTrial(7);  %a74
rev4 = parameterTrial(8);
ROSscale = parameterTrial(9);
%rhoRENscale = parameterTrial(10);  % this is complex 1 concentration %parameter AW 02092017  %%%%%%%%%%
rhoRENscale = simParams.rhoRENscale; %aw added 02092017
fwd5 = 1;
rev5 = 1;
rev6 = 1;

pH_n = -log10(Hm)+3;
pH_p = 7.0;
Hi = 10^(-pH_p+3); %mM
rhoREN = rhoRENscale*0.25;  %0.4; %nmol/mg-protein    %%%%
DpsiB = 50;
%DpsiB = 100; %for goodEnough params
g = 1;
%g = 0.85;

%a12_ =  3.9378e+11*6.3396e+011; %per mM^2, rate change from 6.3396e+027 in 6-proton model
a12_ =  6.3396e+011;
a12 = a12_*Hm^2;
a21 = 5;

a56 = 100;
a65_ =  2.5119e+013; %per mM^2
a65 = rev6*a65_*Hi^2;

a61_ = 1e7;
a61 = fwd5*a61_*exp(-2*.5*g*(Dpsi-DpsiB)*F_over_RT);
a16_ = 130;
a16 = rev5*a16_*exp(2*.5*g*(Dpsi-DpsiB)*F_over_RT);
%remove slip
a25 = 0;
a52 = 0;

%--------------------------------------------------------------------

a23_ = fwd1*3.405e4; %/sqrt(nmol/mg)/s  9.632e5; originally includes half proton
a23 = a23_*sqrt(NADH);
a32 = rev1*8e4;

a34 = fwd2*400;
a43_ = rev2*159;
a43 = a43_*sqrt(NAD); % = 441 for NAD = 7.685nmol/mg-pn;

%a47_ =  fwd3*7.9810e+003;
a47_ =  fwd3*7.9810e+003/.005;   
%a47 = a47_*sqrt(Q)*sqrt(Hm);  %same as original a45 with addition of Q term
%a47 = a47_*sqrt(Q_n)*Hm*rhoRENscale;
a47 = a47_*sqrt(Q_n)*Hm;
a74 = 10*rev3*0.4;  %scale factor is for reversibility
%a74 = 10*rev3*0.4/.005;  %scale factor is for reversibility

a75 = 10*fwd4*40;  %same as original model a45
a57_ = 10*100*.4;  %scale factor is for reversibility
%%%%%%%a57 = rev4*a57_*sqrt(QH2_n);  %same as original model a54 with addition of QH term, moved half proton here    %%%%%%AW
%a57 = rev4*a57_*sqrt(QH2_n)*rhoRENscale;  
a57 = rev4*a57_*sqrt(QH2_n);

%a47 = a47*simParams.rotenoneBlock;
%a57 = a57*simParams.rotenoneBlock;

%AW 06/14/2018
a47 = a47*rhoRENscale;
a57 = a57*rhoRENscale;

E_FMN = -0.375; %V at pH 7.5
E_O2dot = -0.15;
KeqROS = exp(2*(E_O2dot-E_FMN)/RT_over_F);  %approximately 1 -- doesn't have a big effect
a42 = ROSscale*18.5*simParams.O2;  %18.5*O2 matches Lambert and Brand max for fully reduced
a24 = a42*KeqROS*simParams.O2dot;

%this is sort of a hack because we're doing one cycle is one electron, but ROS is produced from fully reduced FMN

%KeqHalfNew = a12_*a23_*a34*a47_*a75*a56*a61_/(a21*a32*a43_*a74*a57_*a65_*a16_)
 
%%
%  numerator
E(1) =  a21*a61*a32*a42*a52*a74+a21*a61*a32*a42*a52*a75+a21*a61*a32*a42*a56*a74+a21*a61*a32*a42*a74*a57+a21*a61*a32*a42*a56*a75+a21*a61*a32*a52*a43*a74+a21*a61*a32*a52*a43*a75+a21*a61*a32*a52*a75*a47+a21*a61*a32*a56*a43*a74+a21*a61*a32*a43*a74*a57+a21*a61*a32*a56*a43*a75+a21*a61*a32*a56*a75*a47+a21*a61*a42*a52*a34*a74+a21*a61*a42*a52*a34*a75+a21*a61*a42*a56*a34*a74+a21*a61*a42*a34*a74*a57+a21*a61*a42*a56*a34*a75+a21*a61*a52*a75*a47*a34+a21*a61*a56*a75*a47*a34+a21*a32*a42*a52*a74*a65+a21*a32*a42*a52*a65*a75+a21*a32*a42*a74*a57*a65+a21*a32*a52*a43*a65*a74+a21*a32*a52*a43*a65*a75+a21*a32*a52*a65*a75*a47+a21*a32*a43*a74*a57*a65+a21*a42*a52*a34*a74*a65+a21*a42*a52*a34*a65*a75+a21*a42*a34*a74*a57*a65+a21*a52*a65*a75*a47*a34+a61*a56*a25*a32*a42*a74+a61*a56*a25*a75*a32*a42+a61*a56*a75*a47*a24*a32+a61*a56*a25*a32*a43*a74+a61*a56*a25*a75*a32*a43+a61*a56*a25*a75*a32*a47+a61*a56*a75*a47*a34*a23+a61*a56*a25*a42*a34*a74+a61*a56*a25*a75*a42*a34+a61*a56*a75*a47*a24*a34+a61*a56*a25*a75*a47*a34; 
E(2) =  a12*a32*a42*a52*a61*a74+a12*a32*a42*a52*a61*a75+a12*a32*a42*a61*a74*a56+a12*a32*a42*a61*a74*a57+a12*a32*a42*a61*a56*a75+a12*a32*a52*a61*a43*a74+a12*a32*a52*a61*a43*a75+a12*a32*a52*a61*a75*a47+a12*a32*a61*a43*a56*a74+a12*a32*a61*a43*a74*a57+a12*a32*a61*a43*a56*a75+a12*a32*a61*a56*a75*a47+a12*a42*a52*a61*a34*a74+a12*a42*a52*a61*a34*a75+a12*a42*a61*a34*a74*a56+a12*a42*a61*a34*a74*a57+a12*a42*a61*a34*a56*a75+a12*a52*a61*a75*a47*a34+a12*a61*a56*a75*a47*a34+a12*a32*a42*a52*a74*a65+a12*a32*a42*a52*a65*a75+a12*a32*a42*a74*a57*a65+a12*a32*a52*a43*a65*a74+a12*a32*a52*a43*a65*a75+a12*a32*a52*a65*a75*a47+a12*a32*a43*a74*a57*a65+a12*a42*a52*a34*a74*a65+a12*a42*a52*a34*a65*a75+a12*a42*a34*a74*a57*a65+a12*a52*a65*a75*a47*a34+a32*a42*a52*a74*a65*a16+a32*a42*a52*a65*a75*a16+a32*a42*a74*a57*a65*a16+a32*a52*a43*a65*a74*a16+a32*a52*a43*a65*a75*a16+a32*a52*a65*a75*a16*a47+a32*a43*a74*a57*a65*a16+a42*a52*a34*a74*a65*a16+a42*a52*a34*a65*a75*a16+a42*a34*a74*a57*a65*a16+a52*a65*a75*a16*a47*a34; 
E(3) =  a23*a12*a42*a52*a61*a74+a23*a12*a42*a52*a61*a75+a23*a12*a42*a61*a74*a56+a23*a12*a42*a61*a74*a57+a23*a12*a42*a61*a56*a75+a23*a43*a12*a52*a74*a61+a23*a43*a12*a52*a61*a75+a23*a12*a52*a61*a75*a47+a23*a43*a12*a74*a61*a56+a23*a43*a12*a74*a61*a57+a23*a43*a12*a61*a56*a75+a23*a12*a61*a56*a75*a47+a43*a24*a74*a12*a52*a61+a43*a24*a12*a52*a61*a75+a43*a24*a74*a12*a61*a56+a43*a24*a74*a12*a57*a61+a43*a24*a12*a61*a56*a75+a43*a74*a57*a25*a12*a61+a43*a74*a57*a65*a16*a21+a23*a12*a42*a52*a74*a65+a23*a12*a42*a52*a65*a75+a23*a12*a42*a74*a57*a65+a23*a43*a12*a52*a74*a65+a23*a43*a12*a52*a65*a75+a23*a12*a52*a65*a75*a47+a23*a43*a12*a74*a57*a65+a43*a24*a74*a12*a52*a65+a43*a24*a12*a52*a65*a75+a43*a24*a74*a12*a57*a65+a43*a74*a57*a25*a65*a12+a23*a42*a52*a74*a65*a16+a23*a42*a52*a65*a75*a16+a23*a42*a74*a57*a65*a16+a23*a43*a52*a74*a65*a16+a23*a43*a52*a65*a75*a16+a23*a52*a65*a75*a16*a47+a23*a43*a74*a57*a65*a16+a43*a24*a74*a52*a65*a16+a43*a24*a52*a65*a75*a16+a43*a24*a74*a57*a65*a16+a43*a74*a57*a25*a65*a16; 
E(4) =  a24*a74*a12*a32*a52*a61+a24*a12*a32*a52*a61*a75+a24*a74*a12*a32*a61*a56+a24*a74*a12*a32*a57*a61+a24*a12*a32*a61*a56*a75+a34*a74*a23*a12*a52*a61+a34*a23*a12*a52*a61*a75+a74*a57*a25*a12*a32*a61+a34*a74*a23*a12*a61*a56+a34*a74*a23*a57*a12*a61+a34*a23*a12*a61*a56*a75+a74*a57*a65*a16*a21*a32+a24*a34*a74*a12*a52*a61+a24*a34*a12*a52*a61*a75+a24*a34*a74*a12*a61*a56+a24*a34*a74*a12*a57*a61+a24*a34*a12*a61*a56*a75+a34*a74*a57*a25*a12*a61+a34*a74*a57*a65*a16*a21+a24*a74*a12*a32*a52*a65+a24*a12*a32*a52*a65*a75+a24*a74*a12*a32*a57*a65+a34*a74*a23*a12*a52*a65+a34*a23*a12*a52*a65*a75+a74*a57*a25*a65*a12*a32+a34*a74*a23*a57*a12*a65+a24*a34*a74*a12*a52*a65+a24*a34*a12*a52*a65*a75+a24*a34*a74*a12*a57*a65+a34*a74*a57*a25*a65*a12+a24*a74*a32*a52*a65*a16+a24*a32*a52*a65*a75*a16+a24*a74*a32*a57*a65*a16+a34*a74*a23*a52*a65*a16+a34*a23*a52*a65*a75*a16+a74*a57*a25*a65*a32*a16+a34*a74*a23*a57*a65*a16+a24*a34*a74*a52*a65*a16+a24*a34*a52*a65*a75*a16+a24*a34*a74*a57*a65*a16+a34*a74*a57*a25*a65*a16; 
E(5) =  a25*a12*a32*a42*a61*a74+a25*a75*a12*a32*a42*a61+a65*a16*a21*a32*a42*a74+a75*a47*a24*a12*a32*a61+a65*a75*a16*a21*a32*a42+a25*a12*a32*a61*a43*a74+a25*a75*a12*a32*a61*a43+a25*a75*a12*a32*a47*a61+a65*a16*a21*a32*a43*a74+a75*a47*a34*a23*a12*a61+a65*a75*a16*a21*a32*a43+a65*a75*a16*a47*a21*a32+a25*a12*a42*a61*a34*a74+a25*a75*a12*a42*a61*a34+a65*a16*a21*a42*a34*a74+a75*a47*a24*a34*a12*a61+a65*a75*a16*a21*a42*a34+a25*a75*a12*a47*a61*a34+a65*a75*a16*a47*a21*a34+a25*a65*a12*a32*a42*a74+a25*a65*a75*a12*a32*a42+a65*a75*a47*a24*a12*a32+a25*a65*a12*a32*a43*a74+a25*a65*a75*a12*a32*a43+a25*a65*a75*a12*a32*a47+a65*a75*a47*a34*a23*a12+a25*a65*a12*a42*a34*a74+a25*a65*a75*a12*a42*a34+a65*a75*a47*a24*a34*a12+a25*a65*a75*a12*a47*a34+a25*a65*a32*a42*a16*a74+a25*a65*a75*a32*a42*a16+a65*a75*a16*a47*a24*a32+a25*a65*a32*a16*a43*a74+a25*a65*a75*a32*a16*a43+a25*a65*a75*a32*a16*a47+a65*a75*a16*a47*a34*a23+a25*a65*a42*a16*a34*a74+a25*a65*a75*a42*a16*a34+a65*a75*a16*a47*a24*a34+a25*a65*a75*a16*a47*a34; 
E(6) =  a16*a21*a32*a42*a52*a74+a16*a21*a32*a42*a52*a75+a16*a56*a21*a32*a42*a74+a16*a21*a32*a42*a74*a57+a16*a56*a21*a75*a32*a42+a16*a21*a32*a52*a43*a74+a16*a21*a32*a52*a43*a75+a16*a21*a32*a52*a75*a47+a16*a56*a21*a32*a43*a74+a16*a21*a32*a43*a74*a57+a16*a56*a21*a75*a32*a43+a16*a56*a21*a75*a32*a47+a16*a21*a42*a52*a34*a74+a16*a21*a42*a52*a34*a75+a16*a56*a21*a42*a34*a74+a16*a21*a42*a34*a74*a57+a16*a56*a21*a75*a42*a34+a16*a21*a52*a75*a47*a34+a16*a56*a21*a75*a47*a34+a56*a25*a12*a32*a42*a74+a56*a25*a75*a12*a32*a42+a56*a75*a47*a24*a12*a32+a56*a25*a12*a32*a43*a74+a56*a25*a75*a12*a32*a43+a56*a25*a75*a12*a32*a47+a56*a75*a47*a34*a23*a12+a56*a25*a12*a42*a34*a74+a56*a25*a75*a12*a42*a34+a56*a75*a47*a24*a34*a12+a56*a25*a75*a12*a47*a34+a16*a56*a25*a32*a42*a74+a16*a56*a25*a75*a32*a42+a16*a56*a75*a47*a24*a32+a16*a56*a25*a32*a43*a74+a16*a56*a25*a75*a32*a43+a16*a56*a25*a75*a32*a47+a16*a56*a75*a47*a34*a23+a16*a56*a25*a42*a34*a74+a16*a56*a25*a75*a42*a34+a16*a56*a75*a47*a24*a34+a16*a56*a25*a75*a47*a34; 
E(7) =  a47*a24*a12*a32*a52*a61+a57*a25*a12*a32*a42*a61+a47*a24*a12*a32*a61*a56+a47*a57*a24*a12*a32*a61+a57*a65*a16*a21*a32*a42+a47*a34*a23*a12*a52*a61+a57*a25*a12*a32*a61*a43+a47*a57*a25*a12*a32*a61+a47*a34*a23*a12*a61*a56+a47*a57*a34*a23*a12*a61+a57*a65*a16*a21*a32*a43+a47*a57*a65*a16*a21*a32+a47*a24*a34*a12*a52*a61+a57*a25*a12*a42*a61*a34+a47*a24*a34*a12*a61*a56+a47*a57*a24*a34*a12*a61+a57*a65*a16*a21*a42*a34+a47*a57*a34*a25*a12*a61+a47*a57*a34*a65*a16*a21+a47*a24*a12*a32*a52*a65+a57*a25*a65*a12*a32*a42+a47*a57*a24*a65*a12*a32+a47*a34*a23*a12*a52*a65+a57*a25*a65*a12*a32*a43+a47*a57*a25*a65*a12*a32+a47*a57*a34*a65*a23*a12+a47*a24*a34*a12*a52*a65+a57*a25*a65*a12*a42*a34+a47*a57*a24*a34*a65*a12+a47*a57*a34*a25*a65*a12+a47*a24*a32*a52*a65*a16+a57*a25*a65*a32*a42*a16+a47*a57*a24*a65*a32*a16+a47*a34*a23*a52*a65*a16+a57*a25*a65*a32*a16*a43+a47*a57*a25*a65*a32*a16+a47*a57*a34*a65*a23*a16+a47*a24*a34*a52*a65*a16+a57*a25*a65*a42*a16*a34+a47*a57*a24*a34*a65*a16+a47*a57*a34*a25*a65*a16; 

% if sum(imag(E)) ~= 0
% 	disp('critical complex I substrates < 0');
% 	pause;
% end

%%
% denominator 
D = E(1) + E(2) + E(3) + E(4) + E(5) + E(6) + E(7); 
%%
% fractions 
F(1) = E(1)/D;
F(2) = E(2)/D;
F(3) = E(3)/D;
F(4) = E(4)/D;
F(5) = E(5)/D;
F(6) = E(6)/D;
F(7) = E(7)/D;

%Jres is electron flux
%It takes two electrons to reduce 1/2 O2 to H2O
%VNO (nmol O/min/mg protein) is approximated by Jres/2
%VO2 ~ Jres/4
%reduction of Q to QH2 also takes two electrons
%dQH/dt = Jres/2 = VNO

%units are now in mM/ms to go with scavenging model
Jres = 1e-3*rhoREN*(F(4)*a47-F(7)*a74);  %measure electron transport outside ROS loop   %AW Jres is the steady-state flux of a single electron through complex I in electrons/min ; Jres = 60*lC1*(F4Pa47 - F7Pa74)

VNO_c1 = Jres/2;  %nmol O/min/mg protein
VROS_c1 = 1e-3*rhoREN*(F(4)*a42-F(2)*a24);

%alternative complex I ROS production based on Kushnareva et al.
%VROS_c1 = 2*1e-6*sqrt(0.0415*NADH/NAD); %unscaled max is 2 at 99% reduced NADH
%VROS_c1 = 4*1e-6*sqrt(0.0415*NADH/NAD); %unscaled max is 2 at 99% reduced NADH

%110112-newerSuccParams
parameterTrial = [99998.1358697277,3640.22935731996,10000,800,100,5000,500,49949.0133694785,250,0.500609565121312,6.30957344480193e-05,1,0.125892541179417,0.501187233627272;];

k03 = parameterTrial(1);
k04 = parameterTrial(2);
k06 = parameterTrial(3);
k07_bLox = parameterTrial(4);
k07_bLred = parameterTrial(5);
k08_bLox = parameterTrial(6);
k08_bLred = parameterTrial(7);
k09 = parameterTrial(8);
V_succ =simParams.V_succ; %AW 021417 %V_succ = parameterTrial(9);
beta = parameterTrial(10);
scale = parameterTrial(11:14);

RT_over_F = 26.708186528497409326424870466321;

%--------complex II----------------

Km_succ = 0.6;
alpha = (1-beta)/2;
gamma = alpha;
%deltas are symmetry factors in that they describe the 
delta1 = 0.5;
delta2 = 0.5;
delta3 = 0.5;

%----------complex III------------------------

%k03 = 500*200;
Keq_3 = exp(-1/RT_over_F*10);
k3 = k03*Keq_3*exp(2.3*(7-pH_p));  %this is correct, 2.3 is change of base from ln to log10
kminus3 = k03;

%Qpdot transfer to bL, bH oxidized
%k04 = 20*200;
Keq_4_bHox = exp(1/RT_over_F*130);
k4_bHox = k04*Keq_4_bHox*exp(-alpha*delta1/RT_over_F*Dpsi);
kminus4_bHox = k04*exp(alpha*(1-delta1)/RT_over_F*Dpsi);

%Qpdot transfer to bL, bH reduced
%k04 = 20*200;
Keq_4_bHred = exp(1/RT_over_F*70);
k4_bHred = k04*Keq_4_bHred*exp(-alpha*delta1/RT_over_F*Dpsi);
kminus4_bHred = k04*exp(alpha*(1-delta1)/RT_over_F*Dpsi);

kd = 1.32e6; %/min

%bL to bH transfter - b2 to b3 and back
%k06 = 20*200;  %-- reducing this moves top corner leftwards
Keq_6 = exp(1/RT_over_F*60);  %was 80, but 60 fits Kim et al exp data
k6=k06*Keq_6*exp(-beta*delta2/RT_over_F*Dpsi);
kminus6 = k06*exp(beta*(1-delta2)/RT_over_F*Dpsi);

%transfer from bH to Qn, bL oxidized
%k07 = 20*1000;
Keq_7_bLox = exp(1/RT_over_F*30);
k7_bLox = k07_bLox*Keq_7_bLox*exp(-gamma*delta3/RT_over_F*Dpsi);
kminus7_bLox = k07_bLox*exp(gamma*(1-delta3)/RT_over_F*Dpsi);

%transfer from bH to Qn, bL reduced
%k07 = 20*1000;
Keq_7_bLred = exp(1/RT_over_F*90);
k7_bLred = k07_bLred*Keq_7_bLred*exp(-gamma*delta3/RT_over_F*Dpsi);
kminus7_bLred = k07_bLred*exp(gamma*(1-delta3)/RT_over_F*Dpsi);

%instead of dismutation
%Qdot_n accept electron from bH, binds two protons to become QH2_n
Keq_8_bLox = exp(1/RT_over_F*130);
k8_bLox = k08_bLox*Keq_8_bLox*exp(2.3*2*(7-pH_n))*exp(-gamma*delta3/RT_over_F*Dpsi);
kminus8_bLox = k08_bLox*exp(gamma*(1-delta3)/RT_over_F*Dpsi);

Keq_8_bLred = exp(1/RT_over_F*60);
k8_bLred = k08_bLred*Keq_8_bLred*exp(2.3*2*(7-pH_n))*exp(-gamma*delta3/RT_over_F*Dpsi);
kminus8_bLred = k08_bLred*exp(gamma*(1-delta3)/RT_over_F*Dpsi);

%reduction of cyt c1 by FeS
%k09 = 500*100; %1000; %--reducing this moves top corner rightwards, makes linear region steeper
Keq_9 = exp(-1/RT_over_F*35);
k9 = k09*Keq_9;
kminus9 = k09;

%k010 = 8*500*0.1; %works well, matches experimental % of respiratory flux well (before SOD stoichiometry change)
%k010 = 500*0.1;  %original version from ETC model figures
k010 = 16*500*0.1; %after SOD stoichiometry fix; for inhbitor titration
%k010 = 12*500*0.1; %for new c1 ROS
Keq_10 = exp(1/RT_over_F*10);
k10 = k010*Keq_10;
kminus10 = k010;

%changed to Kim et al. version so their redox states work
E33_1 = 240; %c1_ox|c1_red
E33_2 = 260; %c_ox|c_red
k33 = 148148;
%kminus33 = 98765;
Keq33 = exp(max(E33_1-E33_2, E33_2-E33_1)/RT_over_F);
kminus33 = k33/Keq33;

%Q_n links complex I to dynamic Q pool
vc1 = VNO_c1;
Ki_OAA = 0.15; %mM
OAA_block = 1/(1+OAA/Ki_OAA);
%OAA_block = 1;
%succON = 0.5/20;  %*3 is for state 3, just for now
succON = 0.085*sqrt(SUC/FUM);
vc2 = OAA_block*1e-3/60*succON*V_succ * Q_n/(Q_n+QH2_n) / (Km_succ + Q_n/(Q_n+QH2_n));
v1 = vc1 + vc2;

v2 = kd*(QH2_n-QH2_p);
v2 = v2/60e3;

v3 = k3*QH2_p*FeS_ox - kminus3*Qdot_p*FeS_red;
v3 = v3/60e3;

v4_bHox = k4_bHox*Qdot_p*b1 - kminus4_bHox*Q_p*b2;
v4_bHred = k4_bHred*Qdot_p*b3 - kminus4_bHred*Q_p*b4;
v4_bHox = 1e-3/60*v4_bHox; %*simParams.myxothiazolBlock;
v4_bHred = 1e-3/60*v4_bHred; %*simParams.myxothiazolBlock;

v5 = 1e-3/60*kd*(Q_p-Q_n);

v6 = k6*b2 - kminus6*b3;
v6 = 1e-3/60*v6;

v7_bLox = k7_bLox*Q_n*b3 - kminus7_bLox*Qdot_n*b1;
v7_bLred = k7_bLred*Q_n*b4 - kminus7_bLred*Qdot_n*b2;
v7_bLox = 1e-3/60*v7_bLox; %*simParams.antimycinBlock;
v7_bLred = 1e-3/60*v7_bLred; %*simParams.antimycinBlock;

%v8 = k8*Qdot_n*Qdot_n - kminus8*Q_n*QH2_n;
v8_bLox = k8_bLox*Qdot_n*b3 - kminus8_bLox*QH2_n*b1;
v8_bLred = k8_bLred*Qdot_n*b4 - kminus8_bLred*QH2_n*b2;
v8_bLox = 1e-3/60*v8_bLox; %*simParams.antimycinBlock;
v8_bLred = 1e-3/60*v8_bLred; %*simParams.antimycinBlock;

v9 = k9*FeS_red*c1_ox - kminus9*FeS_ox*c1_red;
v9 = 1e-3/60*v9;
%v9 = v9*simParams.FeStransferBlock;

v10 = k10*Qdot_p*simParams.O2 - kminus10*Q_p*simParams.O2dot;
v10 = 1e-3/60*v10;
VROS_SS = 2.3e-7; %constitutive ROS, roughly 10% of max ROS (c1+c3)
VROS_SS = VROS_SS*15; %exaggerated for effect to see if this works
VROS_SS = 0;

%reaction 33 -- cytochrome cl reduces cytochome c
v33 = k33*c1_red*c_ox - kminus33*c1_ox*c_red;
v33 = 1e-3/60*v33;
%v33 = v33*simParams.c3Block;

%complex IV


%rates are in mM/min or 1/min
k34 = scale(1)*1000000*2.8e26;
k_34 = scale(1)*10000*275.8e-2;
k35 = scale(2)*1*.5*9e4; %1/mM/min = 1.5e8/M/sec -- Brzezinski and Adelroth has tau = 9us for 1mM O2, seems closest to Sucheta, Szundi et al model from Biochem 1999
k36 = scale(3)*500000*4.6e20;
k_36 = scale(3)*500000*4.6e5;
k37 = scale(4)*3.5e12;
k_37 = scale(4)*3.5e4;
d5 = 0.5;
d6 = 0.5;
 
a12 = k34*exp(-d5*4*Dpsi/RT_over_F)*c_red^3*Hm^4;
a14 = k_37*exp((1-d5)*Dpsi/RT_over_F)*Hi;
a21 = k_34*exp((1-d5)*4*Dpsi/RT_over_F)*c_ox^3*Hi;
a23 = k35*simParams.O2;
%a23 = a23*simParams.CNBlock;
a34 = k36*exp(-d5*3*Dpsi/RT_over_F)*c_red*Hm^3;
a41 = k37*exp(-d5*Dpsi/RT_over_F)*Hm;
a43 = k_36*exp((1-d5)*3*Dpsi/RT_over_F)*c_ox*Hi^2;
 
%%
%  numerator
%state 1 is Y
%state 2 is Yr
%state 3 is YO
%state 4 is YOH
E(1) = a21*a41*a34+a41*a34*a23; 
E(2) = a12*a41*a34; 
E(3) = a23*a12*a41+a43*a14*a21+a23*a43*a12+a23*a43*a14; 
E(4) = a14*a34*a21+a34*a23*a12+a14*a34*a23; 
 
%%
% denominator 
D = E(1) + E(2) + E(3) + E(4); 
%%
% fractions 
Y = E(1)/D;
Yr = E(2)/D;
YO = E(3)/D;
YOH = E(4)/D;

v34 = 1e-3/60*C4_tot*(Y*a12 - Yr*a21);
v35 = 1e-3/60*C4_tot*Yr*a23;
v36 = 1e-3/60*C4_tot*(a34*YO - a43*YOH);
v37 = 1e-3/60*C4_tot*(a41*YOH - a14*Y);

%this is the rate of consumption of reduced cyt c
Ve = 3*v34 + v35;
%this is the number of protons taken out of the matrix, not pumped across because some go to H2O
VHup = 5*VNO_c1+2*v3+4*v34+3*v36+v37;  %zero for complex II -- protons coming off succinate cancel out
%this is the number of protons pumped across the matrix
VH = v34+2*v36+v37;
VO2 = v35;  %v35 is actually the consumption of O2 -- two atoms of oxygen, which requires 4 electrons

%VhRes = 2*v3+VH;
%VNO_c1 is flux of two electrons per minute
%v3 is flux of one electron per minute
%VH is 
VhRes = 4*VNO_c1+2*v3+VH;

%!!! big assumption here for fitting purposes
%v10 =  3.3669e-06;
%VROS_c1 = 0;

%-------------ETC derivatives-----------------------

dQ_n = v5 - v7_bLox - v7_bLred - v1;
dQdot_n = v7_bLox + v7_bLred - v8_bLox - v8_bLred;
dQH2_n = v8_bLox + v8_bLred + v1 -v2;
dQH2_p = v2 - v3;
dQdot_p = v3 -v10 -v4_bHox - v4_bHred;
dQ_p = v10 + v4_bHox + v4_bHred - v5;
db1 = v7_bLox +v8_bLox - v4_bHox;
db2 = v4_bHox + v7_bLred + v8_bLred - v6;
db3 = -v4_bHred + v6 - v7_bLox - v8_bLox;
dFeS_ox = v9 - v3;
dc1_ox = v33 - v9;
dc_ox = Ve - v33;


% =========CK model================
% http://www.ebi.ac.uk/biomodels-main/BIOMD0000000041#metaid_0000015
    x=zeros(10,1);
    x(1)= X(38);  %in mM
    x(2)= X(39);
    x(3)= X(40);
    x(4)= X(41);
    x(5)= X(42);
    x(6)= X(43);
    x(7)= X(44);
    x(8)= X(45);
    x(9)= X(46);
    x(10)= X(47);
    
	compartment_IMS= 0.25;   % Vims; Compartment: id = IMS, name = IMS, constant   %intermembrane space volume, no unit
	compartment_CYT= 3;   %Vcys; % Compartment: id = CYT, name = CYT, constant  %cytosolic compartment volume, no unit

%  Reaction: id = OxPhos, name = Vsyn	, ATP production, flux of ATP entering the IMS from the mitochondria matrix via ANT
% 	reaction_OxPhos_V_1= 1332*1E-6; %uM/s to mM/ms %AW 07102017 %4600.0; % Local Parameter:   id =  V_1, name = Vsynmax
% 	reaction_OxPhos_Ka_1= 35*1E-3; %uM to mM  %AW %800.0; % Local Parameter:   id =  Ka_1, name = Kadp
% 	reaction_OxPhos_Kb_1= 346*1E-3;  %uM to mM   %AW %20.0; % Local Parameter:   id =  Kb_1, name = Kpi
%     reaction_OxPhos=compartment_IMS*reaction_OxPhos_V_1*x(1)*x(9)/(reaction_OxPhos_Ka_1*reaction_OxPhos_Kb_1*(1+x(1)/reaction_OxPhos_Ka_1+x(9)/reaction_OxPhos_Kb_1+x(1)*x(9)/(reaction_OxPhos_Ka_1*reaction_OxPhos_Kb_1))); % AW change unit uM/sec to mM/ms
%     
     
    %reaction_OxPhos
   reaction_OxPhos=VANT; %AW modify 06/2017  @@@@@@@ %reaction_OxPhos=VATPase;
  
    
% Reaction: id = MiCK, name = Vmick	, rate of PCr production via  mitochondrial isoform of CK in IMS
 F_MiCK=simParams.F_MiCK;   %AW 	
 Conc_MiCK=0.25;
 F_MMCK=simParams.F_MMCK;  
 Conc_MMCK=3;
 
    reaction_KeqCK=151.95; %unitless %AW 07/10/2017 152 %equilibrium constant for MiCK and MMCK
    reaction_MiCK_Vf_2= 775*1E-6;  %uM/s to mM/ms %AW %2658.0; % Local Parameter:   id =  Vf_2, name = Vf_2, uM/s
	reaction_MiCK_Kia_2= 751.32*1E-3; %uM to mM  %AW %750.0; 	% Local Parameter:   id =  Kia_2, name = Kia_2, uM, binary dissociation constant ATP
	reaction_MiCK_Kb_2= 5209*1E-3; %uM to mM;  %AW %5200.0; % Local Parameter:   id =  Kb_2, name = Kb_2, uM, ternary dissociation constant PCr
	%reaction_MiCK_Vb_2=11160.0;  % Local Parameter:   id =  Vb_2, name = Vb_2
	reaction_MiCK_Kic_2=201.73*1E-3; %uM to mM;   %AW %204.8;  % Local Parameter:   id =  Kic_2, name = Kic_2
	reaction_MiCK_Kd_2= 499*1E-3; %uM to mM;     %AW %500.0;  % Local Parameter:   id =  Kd_2, name = Kd_2
	reaction_MiCK_Kib_2= 28733*1E-3; %uM to mM;  %AW %28800.0;  % Local Parameter:   id =  Kib_2, name = Kib_2, uM, binary dissociation constant Cr
	reaction_MiCK_Kid_2= 1597*1E-3; %uM to mM;  %AW %1600.0; % Local Parameter:   id =  Kid_2, name = Kid_2
	reaction_MiCK_Vb_2=reaction_MiCK_Vf_2*reaction_KeqCK*reaction_MiCK_Kic_2*reaction_MiCK_Kd_2/(reaction_MiCK_Kia_2*reaction_MiCK_Kb_2);  %AW 07/10/2017
    reaction_MiCK=Conc_MiCK*(F_MiCK*reaction_MiCK_Vf_2*x(2)*x(3)/(reaction_MiCK_Kia_2*reaction_MiCK_Kb_2)-reaction_MiCK_Vb_2*x(1)*x(4)/(reaction_MiCK_Kic_2*reaction_MiCK_Kd_2))/(1+x(3)/reaction_MiCK_Kib_2+x(4)/reaction_MiCK_Kid_2+x(2)*(1/reaction_MiCK_Kia_2+x(3)/(reaction_MiCK_Kia_2*reaction_MiCK_Kb_2))+x(1)*(1/reaction_MiCK_Kic_2+x(3)/(reaction_MiCK_Kic_2*reaction_MiCK_Kib_2)+x(4)/(reaction_MiCK_Kid_2*reaction_MiCK_Kic_2*reaction_MiCK_Kd_2/reaction_MiCK_Kid_2)));
     
   
% Reaction: id = MMCK, name = Vmmck, muscle creatine kinase enzyme in cytosol, rate of PCr  
	reaction_MMCK_Vf_3= 7373.07*1E-6; %uM/s to mM/ms;  %AW %6966.0; % Local Parameter:   id =  Vf_3, name = Vf_3
	reaction_MMCK_Kia_3=1026*1E-3; %uM to mM;  %AW %900.0; % Local Parameter:   id =  Kia_3, name = Kia_3
	reaction_MMCK_Kb_3=16744*1E-3; %uM to mM;  %AW %15500.0; % Local Parameter:   id =  Kb_3, name = Kb_3
	%reaction_MMCK_Vb_3= 29250.0; 	% Local Parameter:   id =  Vb_3, name = Vb_3
	reaction_MMCK_Kic_3=212*1E-3; %uM to mM; %AW %222.4; % Local Parameter:   id =  Kic_3, name = Kic_3
	reaction_MMCK_Kd_3=1669*1E-3; %uM to mM; %AW %1670.0; % Local Parameter:   id =  Kd_3, name = Kd_3
	reaction_MMCK_Kib_3=34504*1E-3; %uM to mM; %AW %34900.0; % Local Parameter:   id =  Kib_3, name = Kib_3
	reaction_MMCK_Kid_3=4516*1E-3; %uM to mM; %AW %4730.0; % Local Parameter:   id =  Kid_3, name = Kid_3
	reaction_MMCK_Vb_3=reaction_MMCK_Vf_3*reaction_KeqCK*reaction_MMCK_Kic_3*reaction_MMCK_Kid_3/(reaction_MMCK_Kia_3*reaction_MMCK_Kb_3);
      reaction_MMCK=Conc_MMCK*(F_MMCK*reaction_MMCK_Vf_3*x(7)*x(8)/(reaction_MMCK_Kia_3*reaction_MMCK_Kb_3)-reaction_MMCK_Vb_3*x(6)*x(5)/(reaction_MMCK_Kic_3*reaction_MMCK_Kd_3))/(1+x(8)/reaction_MMCK_Kib_3+x(5)/reaction_MMCK_Kid_3+x(7)*(1/reaction_MMCK_Kia_3+x(8)/(reaction_MMCK_Kia_3*reaction_MMCK_Kb_3))+x(6)*(1/reaction_MMCK_Kic_3+x(8)/(reaction_MMCK_Kic_3*reaction_MMCK_Kib_3)+x(5)/(reaction_MMCK_Kid_3*reaction_MMCK_Kic_3*reaction_MMCK_Kd_3/reaction_MMCK_Kid_3)));
  
    

% Reaction: id = Pi_diffusion, name = Jpi	
	reaction_Pi_diffusion_k2_5=195/1000; %unit s-1 to ms-1 ; %AW %18.4; % Local Parameter:   id =  k2_5, name = Rpi
   reaction_Pi_diffusion=reaction_Pi_diffusion_k2_5*x(9)-reaction_Pi_diffusion_k2_5*x(10);   %IMS to CYT
   
% Reaction: id = Cr_diffusion, name = Jcr	
	reaction_Cr_diffusion_k1_6=154/1000; %unit s-1 to ms-1 ; %AW %14.6; % Local Parameter:   id =  k1_6, name = Rcr
     reaction_Cr_diffusion=reaction_Cr_diffusion_k1_6*x(3)-reaction_Cr_diffusion_k1_6*x(8);   

% Reaction: id = ADP_diffusion, name = Jadp
	reaction_ADP_diffusion_k1_7= 23.64/1000; %unit s-1 to ms-1 ; %AW %8.16; 	% Local Parameter:   id =  k1_7, name = Radp
    reaction_ADP_diffusion=reaction_ADP_diffusion_k1_7*x(1)-reaction_ADP_diffusion_k1_7*x(6);
  
% Reaction: id = PCr_diffusion, name = Jpcr	
	reaction_PCr_diffusion_k1_8=155.49/1000; %unit s-1 to ms-1 %AW %14.6; % Local Parameter:   id =  k1_8, name = Jpcr
    reaction_PCr_diffusion=reaction_PCr_diffusion_k1_8*x(4)-reaction_PCr_diffusion_k1_8*x(5);
 
    
% Reaction: id = ATP_diffusion, name = Jatp	
	reaction_ATP_diffusion_k1_9=23.64/1000; %unit s-1 to ms-1 %AW %8.16; % Local Parameter:   id =  k1_9, name = Jatp
    reaction_ATP_diffusion=reaction_ATP_diffusion_k1_9*x(2)-reaction_ATP_diffusion_k1_9*x(7);
   
    
% Species:   id = ADPi, name = ADPi, affected by kineticLaw,  xdot(1)
	dADPims = (1/(compartment_IMS))*((-1.0 * reaction_OxPhos) + ( 1.0 * reaction_MiCK) + (-1.0 * reaction_ADP_diffusion)); %dADPims = (-Vsyn +Vmick -Jadp) / Vims;

% Species:   id = ATPi, name = ATPi, affected by kineticLaw, xdot(2)
	dATPims  = (1/(compartment_IMS))*(( 1.0 * reaction_OxPhos) + (-1.0 * reaction_MiCK) + (-1.0 * reaction_ATP_diffusion)); %dATPims = (+Vsyn -Vmick -Jatp) / Vims;
	

% Species:   id = Cri, name = Cri, affected by kineticLaw, xdot(3)
	dCrims = (1/(compartment_IMS))*((-1.0 * reaction_MiCK) + (-1.0 * reaction_Cr_diffusion)); %dCrims = (-Vmick -Jpcr ) / Vims;
	
% Species:   id = PCri, name = PCri, affected by kineticLaw , xdot(4)
	dPCrims = (1/(compartment_IMS))*(( 1.0 * reaction_MiCK) + (-1.0 * reaction_PCr_diffusion));  %dPCrims = ( Vmick -Jpcr ) / Vims;
	
% Species:   id = PCr, name = PCr, affected by kineticLaw, xdot(5)
	dPCr  = (1/(compartment_CYT))*(( 1.0 * reaction_MMCK) + ( 1.0 * reaction_PCr_diffusion)); %dPCr = ( Vmmck +Jpcr ) / Vcyt;
	
% Species:   id = ADP, name = ADP, affected by kineticLaw, xdot(6)
	dADP = (1/(compartment_CYT))*(( 1.0 * reaction_MMCK) + ( 1.0 * reaction_ATPase) + ( 1.0 * reaction_ADP_diffusion));  %dADP = ( Vhyd +Vmmck +Jadp) / Vcyt;
	
% Species:   id = ATP, name = ATP, affected by kineticLaw, xdot(7)
	dATP = (1/(compartment_CYT))*((-1.0 * reaction_MMCK) + (-1.0 * reaction_ATPase) + ( 1.0 * reaction_ATP_diffusion)); %dATP = (-Vhyd -Vmmck +Jatp) / Vcyt;
	
% Species:   id = Cr, name = Cr, affected by kineticLaw, xdot(8)
	dCr = (1/(compartment_CYT))*((-1.0 * reaction_MMCK) + ( 1.0 * reaction_Cr_diffusion)); %dCr = (-Vmmck +Jpcr ) / Vcyt;
	
% Species:   id = Pi, name = Pii, affected by kineticLaw, xdot(9) Pi intermembrane space
      dPiims = (1/(compartment_IMS))*( (-1.0 *reaction_Pi_diffusion)-VPiC) ;  %AW ANT not transport of Pi, it is PiC 07/27/2017
    
% Species:   id = P, name = Pi, affected by kineticLaw, xdot(10)
    dPi = (1/(compartment_CYT))*(( 1.0 * reaction_ATPase) + ( 1.0 * reaction_Pi_diffusion)); 
	
    dATPmito=-VANT+VATPase+VSL;  %AW 07/17/2017
    


%=====================================

%TODO: maybe try to put Vk = VIDH+VKGDH_VMDH-VATPase extra protons back in
%JH=-VHNe - VHSDH + Vhu + VNaH + VPiC + Vhleak; 
JH=-VhRes + Vhu + VNaH + VPiC + Vhleak;  %JH=-VHup + Vhu + VNaH + VPiC + Vhleak; 
dydt=[3.0E-4*(Vuni-VnaCa);              %Cam (1)
VANT-VATPase-VSL;                       %ADPm (2)

%-(-VHNe-VHSDH+Vhu+VANT+Vhleak+VnaCa+2*Vuni+VIMAC)/1.812E-3;% Dpsim (3)
%-VNO+VIDH+VKGDH+VMDH-VTHD;              %NADHm (4)
-(-VhRes+Vhu+VANT+Vhleak+VnaCa+2*Vuni+VIMAC)/1.812E-3;% Dpsim (3)
-Jres/2+VIDH+VKGDH+VMDH-VTHD;              %NADHm (4)

beta_matr*(JH);                         %Hm     (5)
-VATPase+VPiC-VSL;                      %Pim    (6)
VACO-VIDH-VIDH_NADP;                    %ISO    (7)
VIDH+VAAT-VKGDH+VIDH_NADP;              %aKG    (8)
VKGDH-VSL;                              %SCoA   (9)
%VSL-VO2SDH;                             %Succ   (10)
VSL-vc2;
%VO2SDH-VFH;                             %FUM    (11)
vc2-VFH;
VFH-VMDH;                               %MAL    (12)
VMDH-VCS-VAAT;                          %Oaa    (13)
VIDH_NADP+VTHD-VGRm-VTxRm;              %NADPH  (14)
%shunt*(VNO+VO2SDH)-VMnSOD-VtrROS;       %SO2m (15)
%VROS_c1+v10-VMnSOD-VtrROS;
VROS_SS+VROS_c1+v10-VMnSOD-VtrROS;
Vm/Vem*VtrROS-VCuZnSOD;                 %SO2i   (16)
%4e-6-VdifH2O2-VGPXm-VTxPXm;           %H2O2m  (17)
0.5*VMnSOD-VdifH2O2-VGPXm-VTxPXm;           %H2O2m  (17)
0.5*VCuZnSOD+Vm/Vem*VdifH2O2-VGPX-VTxPX-Vcat;  %H2O2i  (18)
% VMnSOD-VdifH2O2-VGPXm-VTxPXm;           %H2O2m  (17)
% VCuZnSOD+Vm/Vem*VdifH2O2-VGPX-VTxPX-Vcat;  %H2O2i  (18)
VGRm-VGPXm-VGRXm+VGST-VPSSGm;           %GSHm   (19)
VGR-VGPX-VGRX-Vm/Vem*VGST-VPSSGi;       %GSHi   (20)
5.0D-1*(VGPXm-VGRm)+VGRXm;              %GSSGm  (21)   
VTxRm-VTxPXm;                           %TrxSH2m (22)
VTxR-VTxPX;                           %TrxSH2 (23)
VPSSGm-VGRXm;                           %PSSGm (24)
VPSSGi-VGRX;                            %PSSGi (25)
dQ_n;   %(26)
dQdot_n; %(27)
dQH2_n;  %(28)
dQH2_p;  %(29)
dQdot_p; %(30)
dQ_p;    %(31)
db1;     %(32)
db2;    %(33)
db3;    %(34)
dFeS_ox;    %(35)
dc1_ox; %(36)
dc_ox;  %(37)


%===CK model==

dADPims; %(38) 
dATPims; %(39)
dCrims; %(40) 
dPCrims; %(41) 
dPCr; %(42) 
dADP; %(43) 
dATP; %(44)
dCr; %(45) 
dPiims; %(46)
dPi; %(47)
dATPmito; %(48)



];



VO2_array=[VO2_array; VO2];
VATPase_array2=[VATPase_array2; VATPase];   % mitochondrial ATP synthesis
VATPase_array=[VATPase_array; reaction_OxPhos]; % ATP synthesis and transport to ims VANT
Time_array=[Time_array; t];
Vhyd_array=[Vhyd_array;reaction_ATPase]; %cytosolic ATP hydroplysis 
VMiCK_array=[VMiCK_array;reaction_MiCK]; %MitoCK rate 
VMMCK_array=[VMMCK_array;reaction_MMCK]; %cytoCK rate 
Cai_array =[Cai_array; Cai];
PCrDiff_array=[PCrDiff_array; reaction_PCr_diffusion];
ATPDiff_array=[ATPDiff_array; reaction_ATP_diffusion];

return
