Supplementary codes for the manuscript entitled "Mitochondrial CaMKII causes metabolic reprogramming, energetic insufficiency, and dilated cardiomyopathy"
by Elizabeth D. Luczak, Yuejin Wu, Jonathan M. Granger, Mei-ling A. Joiner, Nicholas R. Wilson, Ashish Gupta, Amin Sabet, Eleonora Corradini, Wen-Wei Tseng, Yibin Wang, Albert Heck, An-Chi Wei, Robert G. Weiss, Mark E. Anderson

URL: https://gitlab.com/MitoModel/mtCaMKII/

The Matlab codes contain:
Main.m            % script to run the simulations in control, mtCaMKII, mtCaMKIIxCKmt, CKmt groups;
MitoModel.m       % the mitochondria model;
getSimParams.m    % set up the the parameters;

The Matlab codes were run in MathWorks MATLAB 2018a in Windows10.

How to run:
Use "Main.m" to start running the code.
