simParams = struct;
simParams.RT_over_F = 26.708186528497409326424870466321;  %T = 37degC
simParams.pH_n = 7.3;
simParams.pH_p = 7.0;
simParams.rhoSDH=0.017;            
simParams.rhoRENscale=22.1;        
%AW 
simParams.KCS=2.3523e-04;           
simParams.kfACO= 1.1688e-04;  
simParams.kfidh=11.8800;          
simParams.kfKG=0.0132;           
simParams.KfSL=0.0280;         
simParams.kfFH=0.0083;          
simParams.kfMDH=0.1242;        
simParams.kfAAT=0.0214; 
simParams.V_succ=250;              
      

simParams.O2 = 6e-3;
simParams.O2dot = 0.01e-3;

simParams.DpsiClamp = -1;
simParams.NADH = 2.3055;
simParams.NAD = 7.685;
simParams.succON = 0.2;
simParams.gh = 2e-6;


simParams.F_MiCK=25;   %AW 
simParams.F_MMCK=12;    %AW 
simParams.ATPhydAmp= 4E-3;  %AW 
simParams.CaiAmp= 4E-4; 
simParams.CaiRest=1E-4;  

simParams.AcCoA = .1;
simParams.GLU = 30;

simParams.Etxrm=3.5e-1*0.00035;
simParams.Etxr=3.5e-1*0.00035;
% simParams.Etxrm=1e-9;  %for scavenging inhibition
% simParams.Etxr=1e-9;  %for scavenging inhibition
simParams.EtGR= 2.5e-1*9e-4; 
simParams.EtGRm= 2.5e-1*9e-4; 

